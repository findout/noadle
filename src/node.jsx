export default function Node({id, x, y}) {
  const position = {left: `${x}px`, top: `${y}px`};
  return <div id={id} title="node" className="node" style={position}>node {id}</div>;
}