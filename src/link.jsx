import {useState, useEffect} from 'react';

export default function Link({from, to}) {
  const [fromToCoords, setFromToCoords] = useState(null);

  useEffect(()=>{
    setFromToCoords({...getNodeCenter(from, 1), ...getNodeCenter(to, 2)});
  })

  return fromToCoords && <line className="link" title="link" {...fromToCoords}>
      <title>link</title>
    </line>;
}

function getNodeCenter(id, no) {
  console.log("id", id);
  const nodeEl = document.getElementById(id);
  if (!nodeEl) {
    return null;
  }
  const bounds = nodeEl.getBoundingClientRect();
  const coords = {};
  coords[`x${no}`] = bounds.x + bounds.width / 2;
  coords[`y${no}`] = bounds.y + bounds.height / 2;
  console.log("coords", coords);
  return coords;
}