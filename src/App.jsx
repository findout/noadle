import { useState } from 'react';
import Diagram from './diagram';
import Node from './node';
import Link from './link';
import './App.css'

function App() {
  const [aPos, setAPos] = useState({x:10, y:10})
  const [bPos, setBPos] = useState({x:100, y:120})

  return (
    <div className="App">
      <Diagram>
        <Node id="a" {...aPos}/>
        <Node id="b" {...bPos}/>
        <svg>
          <Link from="a" to="b"/>
        </svg>
      </Diagram>
      <button onClick={()=>setAPos({x:10, y:30})}>move a</button>
      <button onClick={()=>setBPos({x:100, y:150})}>move b</button>
    </div>
  )
}

export default App
