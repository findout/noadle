import './diagram.css'

export default function Diagram({children}) {
  return <div className="diagram">{children}</div>;
}